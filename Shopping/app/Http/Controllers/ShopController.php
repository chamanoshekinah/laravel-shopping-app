<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//import the model
use App\Models\Shops;

class ShopController extends Controller
{

    
    public function getAllShops(Request $req){
        $response =Shops::all();
        return $response;

        //to get the data based on the id
        // $response =Shops::where('id',2)->get();
        // return $response;
    }


    //to create new shops to the database
    public function createShops(Request $req){
        $shop = new Shops();
        $shop->name = $req->get('name');
        $shop->location = $req->get('location');
        $shop->save();

        if($shop->id){
            return array('msg'=>"Shop is created", 'id'=>$shop->id);
        }else{
            return array('msg'=>"failed to create a Shop");
        }
    }



    public function updateShops($id, Request $req){
        $shop = Shops::find($id);
        $shop->name = $req->get('name');
        $shop->location = $req->get('location');
        $shop->save(); 

        if($shop->id){
            return array('msg'=>"Shop is Updated", 'id'=>$shop->id);
        }else{
            return array('msg'=>"failed to Update a Shop");
        }
    }


     public function deleteShop($id, Request $req){
        $shop = Shops::find($id);
        $shop->delete();

        if($shop){
            return array('msg'=>"Shop is successfuly deleted");
        }else{
            return array('msg'=>"failed to delete a Shop");
        }
     }
}
