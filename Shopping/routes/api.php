<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//import path to controller
use App\Http\Controllers\ShopController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/allStores', [ShopController::class, 'getAllShops']);
Route::post('/newStores', [ShopController::class, 'createShops']);
Route::put('/updateStores/{id}', [ShopController::class, 'updateShops']);
Route::delete('/eraseStores/{id}', [ShopController::class, 'deleteShop']);


// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });



 
