<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Str;


class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            
            'shop_id' => 1,
            'user_id' => 1,
            'name' => Str::random(10),
            'gender' => 'Male'
        ]);
        DB::table('employees')->insert([
            'shop_id' => 2,
            'user_id' => 2,
            'name' => Str::random(10),
            'gender' => 'Female'
        ]);
        DB::table('employees')->insert([
            'shop_id' => 1,
            'user_id' => 3,
            'name' => Str::random(10),
            'gender' => 'Male'
        ]);
        DB::table('employees')->insert([
            'shop_id' => 2,
            'user_id' => 3,
            'name' => Str::random(10),
            'gender' => 'Female'
        ]);
    }
}
